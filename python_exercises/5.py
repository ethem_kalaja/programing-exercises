# Write a program to sort an integer array without using API methods.

array = [2 ,8 ,5 ,1 ,2 ,7]
min = 0
tmp = 0
for i in range(6):
    min = array[i]
    for j in range(i+1,6):
        if min > array[j]:
            min = array[j]
            tmp = array[i]
            array[i] = min
            array[j] = tmp

print(array)