'''
The prime 41, can be written as the sum of six consecutive primes:
41 = 2 + 3 + 5 + 7 + 11 + 13
This is the longest sum of consecutive primes that adds to a prime below one-hundred.
The longest sum of consecutive primes below one-thousand that adds to a prime,
contains 21 terms, and is equal to 953.
Which prime, below one-million, can be written as the sum of the most consecutive
primes?
'''

primeNumbs = [2]
consecutivePrimes = []
prime = True
x = 0
count = 0
a = 0
max = 0
county = 0

# funksioni qe gjen te gjithe numrat prim
for n in range(3, 4*10**3, 2):

    prime = True
    m = int(n/2)

    for j in range(2, m):
        if n % j == 0:
            prime = False
            break

    if prime:
        primeNumbs.append(n)
        count += 1

# Si funksionon:
# Perderisa deri ne 1000 numri i mbledhjeve eshte 21 atehere ky eshte edhe numri minimal i mbledhjeve qe mund te jet me i madhi.
# Nese e pjestojme 10^6 me 21 del afersisht 50000
# Kjo do te thote qe ne num duhet te gjejme numrat e thjeshte me te medhenj se 50000.
# Kjo na shkurton koh deri ne 100 here
# Bejme te gjitha kombinimet, i hedhim ne nje tabele 2 dimensionale ku ne njeren kolon mban numrin dhe ne tjetrin mban numrin e mbledhjeve
# dhe ne fund i krahasojme per te gjetur numrin me te madh

countx = 0    # numeron se sa eshte numri i numrave prim qe mblidhen
for i in range(count):

    for b in range(i, count):
        prime1 = False      # Perdoret qe edhe nese nuk gjendet numri prim perseri te ndaloje loopin for b
        x += primeNumbs[b]      # x eshte shuma e numrave, primeNumbs tabela qe mban numrat prim
        countx += 1

        if x > 10**6:
            prime1 = True
            x -= primeNumbs[b]
            m = int(x/2)
            prime = True    # Tregon nese nr eshte prime apo jo

            for j in range(2, m):     # Provon nese numri x qe gjetem eshte prim apo jo
                if x % j == 0:
                    prime = False
                    break

            if prime:     # Nese nr eshte prim atehere e fut ne tabelen 2 dimensionale
                consecutivePrimes.append([x, countx-1])
                county += 1     # Numero se sa numra jan ne tabelen consecutivePrimes
            x = 0
            countx = 0

            if prime:
                break

        if prime1:
            break

print(consecutivePrimes)

# Gjen numrin me te madh te serive duke i krahasuar te gjitha serite me njeri tjetrin
for i in range(county-1):
    if consecutivePrimes[i][1] > max:
        max = consecutivePrimes[i][1]
        a = i

print(consecutivePrimes[a])