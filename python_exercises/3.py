# Write a program to check if a number is palindrome
n = int(input("Jepni numrin:"))
T = []
count = 0
isnot = False
x = 0

if n<0:
    print('Numri nuk eshte palindrome.')
else:
    while n >= 1:
        count += 1
        if n/10!=0:
            x = int(n%10)
        T.append(x)
        n /= 10
    if count==1:
        print('Numri eshte palindrome.')
    elif count!=1:
        for j in range (count):
            if T[j] != T[-j-1]:
                isnot = True
                print('Numri nuk eshte palindrome.')
                break

        if isnot!=True:
            print('Numri eshte palindrome.')
