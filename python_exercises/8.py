# Write a program to print Fibonacci series up to given number.
# Write both iterative and recursive version.



memo = {0: 0, 1: 1}
def fibonacciRecursion(n):

    if not n in memo:
        memo[n] = fibonacciRecursion(n - 1) + fibonacciRecursion(n - 2)
    return memo[n]
def fibonacciIteration(n):
    feb = 1
    T = [1]

    for i in range(1, n):
        T.append(feb)
        feb = T[i] + T[i-1]

    return feb

n = int(input('Jepni numrin:'))

print("Numri i fundit febonaci duke perdorur", n, "numra me metoden e rekursiont eshte:",fibonacciRecursion(n))

print("Numri i fundit febonaci duke perdorur", n, "numra me metoden e rekursiont eshte:",fibonacciIteration(n-1))

