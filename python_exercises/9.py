# Write a program to calculate factorial of an integer number.
# Both iterative and recursive solution.

def faktorialRecursion(n):
    if n == 0:
        return 1

    return n * faktorialRecursion(n - 1)

def faktorialIteration(n):
    x = 1
    for i in range(2, n + 1):
        x *= i

    return x

n = int(input('Jepni numrin:'))

print("Faktoriali i numrit ", n , "me rekursion eshte:",
      faktorialRecursion(n))

print("Faktoriali i numrit ", n , "me interations eshte:",
      faktorialIteration(n))
