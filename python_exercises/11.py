'''
Index of 1000 digit Fibonacci number
The Fibonacci sequence is defined by the recurrence relation:
Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:
F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.
What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
'''


def gjejNrFibonacci():
    n = 5000 # Marrim nje numer te cfardoshem por me kusht qe te jete i madh
    fib = 1
    T = [1]
    count = 0
    a = 0
    while a < 1000:
        while fib<n:
            for i in range(1, n):
                T.append(fib)           # Shton numrin fibonacci te radhes ne tabele
                fib = int(T[i] + T[i - 1])         # Gjen numrin fibonacci te rradhes duke mbledhur vleren e tanishme me paradhesen e tabeles
                a = len(str(fib))       # Gjen gjatesine e numrin
                count += 1          # Numeron se sa numra fibonacci jan gjetur
                if a == 1000:       # Numri i pare qe arrin gjatesine 1000 ndalon funksionin
                    return count


print(gjejNrFibonacci())

def prova(count):
    R = [1]
    fibb = 1
    for i in range(1,count+1):
        R.append(fibb)
        fibb = int(R[i] + R[i - 1])
    return fibb

print(prova(gjejNrFibonacci()))