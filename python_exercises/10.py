# Print following structure.
# This program is a good exercise for mastering loops. This also teaches you how to use
# break and continue statement with loops.
# *
# ***
# *****
# ***
# *
n = int(input('Jepni me sa yje e doni lartesine:'))
if n%2==0:
    n -=1

for i in range(1,n,2):
    print("*"*i)

for i in range(n,0,-2):
    print('*'*i)
